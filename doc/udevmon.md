The yaml config file should be put into `/etc/interception/udevmon.d/`

to get the keyboard name use
> sudo libinput list-devices | grep Device | grep eyboard

if this is not working try
> sudo libinput list-devices | grep Device

and lock for anything that could be your keyboard

insert the config and keyboard name. Dont use whitespaces in the config name

for more options see https://gitlab.com/interception/linux/tools

```yaml
- JOB: "intercept -g $DEVNODE | kbdmod -c /etc/interception/kbdmod/config name | uinput -d $DEVNODE"
  DEVICE:
    NAME: "keyboard name"
```

for multiple keyboards 
```yaml
- JOB: "intercept -g $DEVNODE | kbdmod -c /etc/interception/kbdmod/config name | uinput -d $DEVNODE"
  DEVICE:
    NAME: "keyboard1 name"
- JOB: "intercept -g $DEVNODE | kbdmod -c /etc/interception/kbdmod/config name | uinput -d $DEVNODE"
  DEVICE:
    NAME: "keyboard2 name"
- JOB: ...
```