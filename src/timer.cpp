#include "timer.h"
#include "mapping.h"

extern bool running;
extern std::mutex layer_mutex;
TimerEvent timer_event;

milliseconds delay;

void TimerLoop()
{
	while (running)
	{
		// check if queue is empty
		if (timer_event.is_empty())
		{
			sleep_for(delay);
			continue;
		}
		sleep_until(timer_event.execution_time);
		timer_event.mutex.lock();
		if(timer_event.reset || timer_event.is_empty()) {
			timer_event.reset = false;
			timer_event.mutex.unlock();
			continue;
		}
		auto current = timer_event.m;
		timer_event.m = nullptr;
		timer_event.mutex.unlock();

		// execute event
		layer_mutex.lock();
		current->timeout_event();
		layer_mutex.unlock();
	}
}
