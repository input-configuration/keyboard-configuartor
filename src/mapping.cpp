#include "mapping.h"
#include "mapping_out_const.h"

mapping* AktiveKey = nullptr;
extern TimerEvent timer_event;
extern milliseconds delay;
extern IOTYPE IO;
OutputStorage SHIFT_OUTPUT;

mapping::~mapping() {
	output.destruct();
}

void mapping::release() {
	timer_event.clear();
	if(AktiveKey != nullptr && AktiveKey != this)
		AktiveKey->consume_event();

	//check key state
	switch(key & KEY_STATE) {
	case TAP_OUTPUT_PRESSED_MASK:
		write_output_release<tapT>();
		break;
	case HOLD_OUTPUT_PRESSED_MASK:
		write_output_release<holdT>();
		break;
	case DOUBLETAP_OUTPUT_PRESSED_MASK:
		write_output_release<doubletapT>();
		break;
	case TAPHOLD_OUTPUT_PRESSED_MASK:
		write_output_release<tapholdT>();
		break;
	case DOUBLE_PRESS_MASK:
		write_output<doubletapT>();
		break;
	case SINGLE_PRESS_MASK:
	 	//check for doubletap or taphold
		if(key & DOUBLETAP_ENABLED_MASK + TAPHOLD_ENABLED_MASK) {
			key ^= DOUBLE_PRESS_MASK;
			timer_event.set(delay, this);
		}else{
			write_output<tapT>();
		}
	}
}

void mapping::press() {

	if(key & OUTPUT_PRESSED_MASK)
		fprintf(stderr, "Error: key already pressed\n");

	//check if only tap is enabled
	if(!(key & (TAPHOLD_ENABLED_MASK + HOLD_ENABLED_MASK + DOUBLETAP_ENABLED_MASK))) {
		write_output_press<tapT>();
		return;
	}

	timer_event.set(delay, this);

	//check key state
	if(key & KEY_STATE) {
		if(key & TAPHOLD_ENABLED_MASK) {
			key |= DOUBLE_PRESS_MASK;
		}else{
			write_output_press<doubletapT>();
			timer_event.clear();
			return;
		}
	}

	AktiveKey = this;
	key |= SINGLE_PRESS_MASK;
}

void mapping::consume_event() {
	timer_event.clear();
	//check key state
	if ((key & DOUBLE_PRESS_MASK) == 3) {
		write_output<doubletapT>();
		reset();
	}else if(key & SINGLE_RELEASE_MASK) {
		write_output<tapT>();
		reset();
	}else{
		write_output_press<tapT>();
		reset();
		key |= TAP_OUTPUT_PRESSED_MASK;
	}
}

void mapping::timeout_event() {
	//check key state
	switch(key & KEY_STATE) {
	case SINGLE_PRESS_MASK:
		if(key & HOLD_ENABLED_MASK) {
			if(key & AUTOSHIFT_CAPABLE_MASK) {
				reset();
				AktiveKey = nullptr;
				IO.write_event_press(SHIFT_OUTPUT);
				IO.write_event_press(tap());
				IO.write_event_release(tap());
				IO.write_event_release(SHIFT_OUTPUT);
				return;
			}

			write_output_press<holdT>();
			key |= HOLD_OUTPUT_PRESSED_MASK;
			return;
		}
		write_output_press<tapT>();
		key |= TAP_OUTPUT_PRESSED_MASK;
		return;
	case SINGLE_RELEASE_MASK:
		write_output<tapT>();
		break;
	case DOUBLE_PRESS_MASK:
		write_output_press<tapholdT>();
		key |= TAPHOLD_OUTPUT_PRESSED_MASK;
		return;
	}
}
