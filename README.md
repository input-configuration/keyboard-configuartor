# kbdmod

[![pipeline status](https://gitlab.com/calvinreu/kbdmod/badges/main/pipeline.svg)](https://gitlab.com/calvinreu/kbdmod/-/commits/main)
[![AUR](https://img.shields.io/aur/version/kbdmod)](https://aur.archlinux.org/packages/kbdmod)
[![Latest Release](https://gitlab.com/calvinreu/kbdmod/-/badges/release.svg)](https://gitlab.com/calvinreu/kbdmod/-/releases)

intercept keycodes and change them to emulate qmk/oryx zsa like behaviour with any keyboard meaning you can assign macros, OSMs and normal keys to single tap, double tap, hold or tap and then hold of a key. Create multiple keyboard layers and cycle between them or enable autoshift to automaticly add a shift key for normal keys which are held down to improve writing comfort.

this project is inspired by [dual-function-keys](https://gitlab.com/interception/linux/plugins/dual-function-keys)

## Installation
dependencies
 - yaml-cpp
 - evdev
 - interception-tools

build dependencies
 - cmake
 - make

 > cd tmp && git clone https://gitlab.com/calvinreu/kbdmod && cd kbdmod && ./install.sh

## Configuration
There are two parts to be configured: kbdmod and udevmon, which launches kbdmod.

[how to configure kbdmod](https://gitlab.com/calvinreu/kbdmod/-/blob/main/doc/kbdmod.md)

[examples](https://gitlab.com/calvinreu/kbdmod/-/blob/main/examples) which contains kbdmod configurations.

### udevmon
#### auto setup
if you only have one keyboard connected (on laptop none) then you can run
 > sudo templates/intercept-setup.sh

in case you get an error stating sed -e and so on edit the /etc/interception/udevmon.d/udevmon.yaml with the text editor of your choice and replace keyboard name with the device name

this will create and init the udevmon config file. The config file name asked for when running this script is the full name of the kbdmod config file you can choose this name yourself but it has to match the config file you create later on.
#### manual config
[doc](https://gitlab.com/calvinreu/kbdmod/-/blob/main/doc/udevmon.md)

## Enabling kbdmod
to test if everything is working properly run
 > sudo systemctl start udevmon

check if the keyboard is working as intended if so
 > sudo systemctl enable udevmon

if it is not working as intended run journalctl -u udevmon and check for errors in the logs. In case you are unable to resolve the issue on your own feel free to create an issue with the logs and all the files contained in /etc/interception/.
NOTE: if kbdmod does not change any behaviour and the logs suggest that it started without any issues check if the device name in the udevmon.yaml in /etc/interception/udevmon.d exactly matches the keyboard device name

## Disabling kbdmod
for the current session
 > sudo systemctl stop udevmon

permanantly
 > sudo systemctl disable udevmon

## Usage
to get a bit of inspiration what you could do with this look for zsa oryx or qmk on the internet

## encryted setups
#### NOTE: all of this is not actualy tested so it probably wont work do not expect this to be worked on quickly
### installation
to start kbdmod before entering the decryption password
 > cp ./initcpioinstall /etc/initcpio/install/kbdmod && cp ./initcpiohook /etc/initcpio/hooks

add kbdmod to the line HOOKS=(...) in /etc/mkinitcpio.conf

copy your config files to /etc/interception

build the binary to /bin
 > $ sudo ./install.sh "--DESTDIR=/bin"

create initramfs
 > $ sudo mkinitcpio -P

### update
to update the config or the binary
 > $ sudo mkinitcpio -P

## Support
Just create an issue or mail me

## Contributing
Contributions are very welcome for improvements. The most obvious bugs can be catched by running the script test/test.sh but please test your code by installing it to your system disabling udevmon then starting it and if it works properly reanabling it

PS: please follow the editorconfig

## Debuging
to debug build the project with the DEBUG flag in cmake
use the console and the sequences from test/combinations.txt for testing
this is a part of the vscode debuger options but they should be easy to convert to any other debuger as well
```json
"program": "${workspaceFolder}/build/kbdmod.bin",
"args": ["-c", "${workspaceFolder}/test/testing.yaml"],
```

## License
this project is under the MIT license (c)Calvin Reu see LICENSE

## Project status
this is a stable version I use it every day since there are no users I know of there will probably be bugs

## Roadmap
 - add mouse click as commands
 - config creator would be nice if it has a gui web based

